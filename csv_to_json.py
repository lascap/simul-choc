import json
from os import path
import sys

import pandas


def _convert_csvs(csv_filenames):
    datasets = {}
    for csv_filename in csv_filenames:
        filename = csv_filename.replace('csv', 'json')
        dataset_name = path.basename(filename).split('.')[0]
        datasets[dataset_name] = filename
        try:
            values = pandas.read_csv(csv_filename, names=['a', 'b', 'c']).c.tolist()
            with open(filename, 'w') as json_file:
                json.dump([round(v * 10000) / 10000 for v in values], json_file)
        except Exception as err:
            print('Error while converting file {}'.format(csv_filename))
            raise err

    with open('index.js', 'w') as index_file:
        for i, k in enumerate(sorted(datasets.keys())):
            index_file.write("import dataset{} from './{}'\n".format(i, datasets[k]))
        index_file.write('\n\nexport default [\n')
        for i, k in enumerate(sorted(datasets.keys())):
            patient_num = int(k[len('patient '):])
            index_file.write(
                "  {{name: '{}', data: dataset{}, num: {}}},\n".format(k, i, patient_num))
        index_file.write(']\n')


if __name__ == '__main__':
    _convert_csvs(sys.argv[1:])
