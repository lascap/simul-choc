import PropTypes from 'prop-types'
import React from 'react'

import {Button} from './button'


class LandingPage extends React.Component {
  static propTypes = {
    onNext: PropTypes.func.isRequired,
    style: PropTypes.object,
    title: PropTypes.string,
    translate: PropTypes.func.isRequired,
  }

  renderKind() {
    const {title} = this.props
    if (title) {
      return title
    }
  }

  render() {
    const {onNext, style, translate} = this.props
    const pageStyle = {
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'center',
      ...style,
    }
    const contentStyle = {
      alignItems: 'center',
      display: 'flex',
      flexDirection: 'column',
    }
    return <div style={pageStyle}>
      <div style={contentStyle}>
        <img
          src={translate('logo-svg')}
          alt={translate('Simul Shock')} />
        {this.renderKind()}
        <Button onClick={onNext} style={{marginTop: 37}}>
          {translate('Start')}
        </Button>
      </div>
    </div>
  }
}


export {LandingPage}
