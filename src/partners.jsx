import PropTypes from 'prop-types'
import React from 'react'

import logoBSPP from 'images/logo-bspp.png'
import logoBayesImpact from 'images/logo-bayesimpact.png'
import logoSSA from 'images/logo-ssa.jpg'


const partnerStyle = {
  alignItem: 'center',
  backgroundColor: 'white',
  display: 'flex',
  height: 140,
  justifyContent: 'center',
  margin: 20,
  width: 140,
}
const imageStyle = {
  maxHeight: '100%',
  maxWidth: '100%',
}


const PartnerBase = ({src, alt}) => <div style={partnerStyle}>
  <img alt={alt} src={src} style={imageStyle} title={alt} />
</div>
PartnerBase.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
}
const Partner = React.memo(PartnerBase)


const containerStyle = {
  display: 'flex',
  justifyContent: 'center',
}
const PartnersBase = () => <div style={containerStyle}>
  <Partner src={logoBSPP} alt="Brigade des Sapeurs-Pompiers de Paris" />
  <Partner src={logoSSA} alt="Service de Santé des Armées" />
  <Partner src={logoBayesImpact} alt="Bayes Impact" />
</div>
const Partners = React.memo(PartnersBase)


export {Partners}
