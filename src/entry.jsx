import React from 'react'
import ReactDOM from 'react-dom'
import {AppContainer} from 'react-hot-loader'

import {App} from './app'

const appDom = document.createElement('div')
document.body.appendChild(appDom)
ReactDOM.render(<AppContainer><App /></AppContainer>, appDom)


if (module.hot) {
  module.hot.accept('./app', () => {
    const UpdatedApp = require('./app').App
    ReactDOM.render(<AppContainer><UpdatedApp /></AppContainer>, appDom)
  })
}


function init() {
  Array.from(document.getElementsByTagName('title')).
    forEach(titleElement => titleElement.innerText = 'Simul Choc')
}
init()
