import PropTypes from 'prop-types'
import React from 'react'

import amorceData from './amorce.json'


class Screen extends React.Component {
  static propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
    dataPeriod: PropTypes.number.isRequired,
    drawPeriodSeconds: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    onDataEnd: PropTypes.func,
    scaleMillimetersPerMillivolt: PropTypes.number.isRequired,
    startingDurationSeconds: PropTypes.number.isRequired,
    waitingData: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
    waitingDataPeriod: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    widthPeriodSeconds: PropTypes.number.isRequired,
  }

  static defaultProps = {
    backgroundColor: '#030d03',
    color: '#0a0',
    drawPeriodSeconds: .01,
    scaleMillimetersPerMillivolt: 10,
    startingDurationSeconds: .2,
    waitingData: amorceData.map(val => val * .003),
    waitingDataPeriod: .008,
    widthPeriodSeconds: 6,
  }

  state = {
    cursorTop: 0,
    hasStarted: false,
    testTime: 0,
    waitingTime: 0,
  }

  componentWillMount() {
    this.startWaiting()
  }

  componentDidMount() {
    const {backgroundColor, color, height, width} = this.props
    this.canvas.fillStyle = backgroundColor
    this.canvas.fillRect(0, 0, width, height)
    this.canvas.strokeStyle = color
    this.canvas.lineWidth = 1.5
  }

  componentWillUpdate(nextProps) {
    const {data, dataPeriod} = this.props
    if (data !== nextProps.data || dataPeriod !== nextProps.dataPeriod) {
      this.startWaiting()
      this.componentDidMount()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {cursorTop, waitingTime} = this.state
    if (!this.canvas || !this.state.waitingTime) {
      return
    }
    const {height, width} = this.props
    const [prevX, prevY] = this.convertCoordinates(prevState)
    const [x, y] = this.convertCoordinates({cursorTop, waitingTime})

    const cleanBufferWidth = Math.ceil((x - prevX) * 10) + 1
    this.canvas.clearRect(Math.floor(x), 0, cleanBufferWidth + 1, height)
    this.canvas.fillRect(Math.floor(x), 0, cleanBufferWidth + 1, height)

    this.canvas.beginPath()
    this.canvas.moveTo(prevX, prevY)
    if (x < prevX) {
      this.canvas.lineTo(x + width, y)
      this.canvas.moveTo(prevX - width, prevY)
    }
    this.canvas.lineTo(x, y)
    this.canvas.stroke()

    if (x + cleanBufferWidth > width) {
      this.canvas.clearRect(0, 0, x + cleanBufferWidth - width, height)
      this.canvas.fillRect(0, 0, x + cleanBufferWidth - width, height)
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  convertCoordinates({cursorTop, waitingTime}) {
    const {height, scaleMillimetersPerMillivolt, width, widthPeriodSeconds} = this.props
    // Usual screens have 72dpi.
    const pixelsPerMillimeters = 72 / 25.4
    const cursorLeft = (waitingTime / 1000) / widthPeriodSeconds % 1
    return [
      cursorLeft * width,
      height / 2 - cursorTop * scaleMillimetersPerMillivolt * pixelsPerMillimeters,
    ]
  }

  getWaitingValue(waitingTime) {
    const {waitingDataPeriod} = this.props
    const {waitingData} = this.state
    const dataIndex = Math.round((waitingTime / (waitingDataPeriod * 1000)) % waitingData.length)
    return waitingData[dataIndex]
  }

  getInterpolatedValue(testTime, waitingTime) {
    if (!testTime) {
      return this.getWaitingValue(waitingTime)
    }

    const {data, dataPeriod, startingDurationSeconds} = this.props
    const dataIndex = Math.round((testTime / (dataPeriod * 1000)))
    if (dataIndex >= data.length) {
      throw 'End of the test data.'
    }
    const value = data[dataIndex]

    const startingWeight = testTime / startingDurationSeconds / 1000
    if (startingWeight >= 1) {
      return value
    }
    return startingWeight * value + (1 - startingWeight) * this.getWaitingValue(waitingTime)
  }

  start() {
    this.setState({hasStarted: true, testTime: 0})
  }

  startWaiting() {
    const {drawPeriodSeconds, onDataEnd, waitingData} = this.props

    this.stop()
    this.setState({
      cursorTop: 0,
      hasStarted: false,
      testTime: 0,
      waitingTime: 0,
    })

    this.interval = setInterval(() => {
      const {hasStarted} = this.state
      const waitingTime = this.state.waitingTime + drawPeriodSeconds * 1000
      const testTime = this.state.testTime + (hasStarted ? (drawPeriodSeconds * 1000) : 0)
      try {
        this.setState({
          cursorTop: this.getInterpolatedValue(testTime, waitingTime),
          testTime,
          waitingTime,
        })
      } catch (unusedError) {
        this.stop()
        onDataEnd && onDataEnd()
      }
    }, drawPeriodSeconds * 1000)

    // Smooth waiting data so that it can repeat itself as much as needed.
    const smoothCount = 64
    this.setState({waitingData:
      waitingData.slice(0, -smoothCount).map((data, index) => {
        if (index >= smoothCount) {
          return data
        }
        const weight = index / smoothCount
        const previousData = waitingData[waitingData.length - smoothCount + index]
        return data * weight + previousData * (1 - weight)
      }),
    })
  }

  stop() {
    clearInterval(this.interval)
    return this.state.testTime
  }

  render() {
    const {height, width} = this.props
    const assignRef = dom => {
      this.canvas = dom && dom.getContext('2d')
    }
    return <canvas height={height} width={width} ref={assignRef} />
  }
}


export {Screen}
