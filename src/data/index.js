import dataset0 from './patient 1.json'
import dataset1 from './patient 10.json'
import dataset2 from './patient 11.json'
import dataset3 from './patient 12.json'
import dataset4 from './patient 13.json'
import dataset5 from './patient 14.json'
import dataset6 from './patient 15.json'
import dataset7 from './patient 16.json'
import dataset8 from './patient 17.json'
import dataset9 from './patient 18.json'
import dataset10 from './patient 19.json'
import dataset11 from './patient 2.json'
import dataset12 from './patient 20.json'
import dataset13 from './patient 21.json'
import dataset14 from './patient 22.json'
import dataset15 from './patient 23.json'
import dataset16 from './patient 24.json'
import dataset17 from './patient 25.json'
import dataset18 from './patient 26.json'
import dataset19 from './patient 27.json'
import dataset20 from './patient 28.json'
import dataset21 from './patient 29.json'
import dataset22 from './patient 3.json'
import dataset23 from './patient 31.json'
import dataset24 from './patient 32.json'
import dataset25 from './patient 33.json'
import dataset26 from './patient 34.json'
import dataset27 from './patient 35.json'
import dataset28 from './patient 36.json'
import dataset29 from './patient 37.json'
import dataset30 from './patient 38.json'
import dataset31 from './patient 39.json'
import dataset32 from './patient 4.json'
import dataset33 from './patient 40.json'
import dataset34 from './patient 41.json'
import dataset35 from './patient 42.json'
import dataset36 from './patient 43.json'
import dataset37 from './patient 44.json'
import dataset38 from './patient 45.json'
import dataset39 from './patient 46.json'
import dataset40 from './patient 47.json'
import dataset41 from './patient 48.json'
import dataset42 from './patient 49.json'
import dataset43 from './patient 5.json'
import dataset44 from './patient 50.json'
import dataset45 from './patient 51.json'
import dataset46 from './patient 52.json'
import dataset47 from './patient 53.json'
import dataset48 from './patient 54.json'
import dataset49 from './patient 55.json'
import dataset50 from './patient 56.json'
import dataset51 from './patient 57.json'
import dataset52 from './patient 58.json'
import dataset53 from './patient 59.json'
import dataset54 from './patient 6.json'
import dataset55 from './patient 60.json'
import dataset56 from './patient 61.json'
import dataset57 from './patient 62.json'
import dataset58 from './patient 63.json'
import dataset59 from './patient 64.json'
import dataset60 from './patient 65.json'
import dataset61 from './patient 66.json'
import dataset62 from './patient 67.json'
import dataset63 from './patient 68.json'
import dataset64 from './patient 7.json'
import dataset65 from './patient 8.json'
import dataset66 from './patient 9.json'


export default [
  {name: 'patient 1', data: dataset0, num: 1},
  {name: 'patient 10', data: dataset1, num: 10},
  {name: 'patient 11', data: dataset2, num: 11},
  {name: 'patient 12', data: dataset3, num: 12},
  {name: 'patient 13', data: dataset4, num: 13},
  {name: 'patient 14', data: dataset5, num: 14},
  {name: 'patient 15', data: dataset6, num: 15},
  {name: 'patient 16', data: dataset7, num: 16},
  {name: 'patient 17', data: dataset8, num: 17},
  {name: 'patient 18', data: dataset9, num: 18},
  {name: 'patient 19', data: dataset10, num: 19},
  {name: 'patient 2', data: dataset11, num: 2},
  {name: 'patient 20', data: dataset12, num: 20},
  {name: 'patient 21', data: dataset13, num: 21},
  {name: 'patient 22', data: dataset14, num: 22},
  {name: 'patient 23', data: dataset15, num: 23},
  {name: 'patient 24', data: dataset16, num: 24},
  {name: 'patient 25', data: dataset17, num: 25},
  {name: 'patient 26', data: dataset18, num: 26},
  {name: 'patient 27', data: dataset19, num: 27},
  {name: 'patient 28', data: dataset20, num: 28},
  {name: 'patient 29', data: dataset21, num: 29},
  {name: 'patient 3', data: dataset22, num: 3},
  {name: 'patient 31', data: dataset23, num: 31},
  {name: 'patient 32', data: dataset24, num: 32},
  {name: 'patient 33', data: dataset25, num: 33},
  {name: 'patient 34', data: dataset26, num: 34},
  {name: 'patient 35', data: dataset27, num: 35},
  {name: 'patient 36', data: dataset28, num: 36},
  {name: 'patient 37', data: dataset29, num: 37},
  {name: 'patient 38', data: dataset30, num: 38},
  {name: 'patient 39', data: dataset31, num: 39},
  {name: 'patient 4', data: dataset32, num: 4},
  {name: 'patient 40', data: dataset33, num: 40},
  {name: 'patient 41', data: dataset34, num: 41},
  {name: 'patient 42', data: dataset35, num: 42},
  {name: 'patient 43', data: dataset36, num: 43},
  {name: 'patient 44', data: dataset37, num: 44},
  {name: 'patient 45', data: dataset38, num: 45},
  {name: 'patient 46', data: dataset39, num: 46},
  {name: 'patient 47', data: dataset40, num: 47},
  {name: 'patient 48', data: dataset41, num: 48},
  {name: 'patient 49', data: dataset42, num: 49},
  {name: 'patient 5', data: dataset43, num: 5},
  {name: 'patient 50', data: dataset44, num: 50},
  {name: 'patient 51', data: dataset45, num: 51},
  {name: 'patient 52', data: dataset46, num: 52},
  {name: 'patient 53', data: dataset47, num: 53},
  {name: 'patient 54', data: dataset48, num: 54},
  {name: 'patient 55', data: dataset49, num: 55},
  {name: 'patient 56', data: dataset50, num: 56},
  {name: 'patient 57', data: dataset51, num: 57},
  {name: 'patient 58', data: dataset52, num: 58},
  {name: 'patient 59', data: dataset53, num: 59},
  {name: 'patient 6', data: dataset54, num: 6},
  {name: 'patient 60', data: dataset55, num: 60},
  {name: 'patient 61', data: dataset56, num: 61},
  {name: 'patient 62', data: dataset57, num: 62},
  {name: 'patient 63', data: dataset58, num: 63},
  {name: 'patient 64', data: dataset59, num: 64},
  {name: 'patient 65', data: dataset60, num: 65},
  {name: 'patient 66', data: dataset61, num: 66},
  {name: 'patient 67', data: dataset62, num: 67},
  {name: 'patient 68', data: dataset63, num: 68},
  {name: 'patient 7', data: dataset64, num: 7},
  {name: 'patient 8', data: dataset65, num: 8},
  {name: 'patient 9', data: dataset66, num: 9},
]
