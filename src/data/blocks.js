import fullData from './index'

// Set of patients for whom experts could not agree on a decision.
const undecidedPatients = new Set([40, 56, 57, 59, 61])

// Set of patients used in the tutorial block.
const tutorialPatients = new Set([3, 23, 4])

const decidedPatients = fullData.filter(({num}) => !undecidedPatients.has(num))

// Split line for patient numbers to separate two blocks.
const splitPatientsAt = 31.5

export default [
  decidedPatients.filter(({num}) => tutorialPatients.has(num)),
  decidedPatients.filter(({num}) => num < splitPatientsAt),
  decidedPatients.filter(({num}) => num > splitPatientsAt),
]
