import {initializeApp} from 'firebase/app'
import {getFirestore, addDoc, getDoc, doc, collection, updateDoc} from 'firebase/firestore'
import Cookies from 'js-cookie'
import PropTypes from 'prop-types'
import React, {useState} from 'react'
import queryString from 'query-string'

const parse = queryString.parse
window.global = window

import chocButtonImage from './images/choc.svg'
import logoImage from './images/logo.svg'
import massageButtonImage from './images/massage.svg'

import realData from './data/index'
import dataBlocks from './data/blocks'
import {BigButton, Button} from './button'
import {Form} from './form'
import {getTranslator} from './i18n'
import {LandingPage} from './landing'
// import {Partners} from './partners'
import {ResultsTable} from './results'
import {Screen} from './screen'

import './styles/app.css'


function shuffleArray(array) {
  const newArray = [...array]
  for (let i = newArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1))
    const oldJValue = newArray[j]
    newArray[j] = newArray[i]
    newArray[i] = oldJValue
  }
  return newArray
}


const allCases = shuffleArray(realData)
const shuffledDataBlocks = dataBlocks.map(shuffleArray)

const blockMessages = [
  'First of all, 3 trails to get acquainted with the simulator.',
  'Are you ready? The simulation for the first 30 patients in cardiac arrest is waiting for your expertise.',
  'Tired? Think it could be 4am after a sleepless night! Blow a few moments and here we go again! Your results are waiting for you at the end.',
]


class ProgressBar extends React.Component {
  static propTypes = {
    max: PropTypes.number.isRequired,
    progress: PropTypes.number.isRequired,
  }

  render() {
    const {max, progress} = this.props
    const containerStyle = {
      backgroundColor: '#fff',
      border: 'solid 1px',
      borderRadius: 5,
      height: 20,
      margin: 15,
      overflow: 'hidden',
      position: 'relative',
    }
    const progressStyle = {
      backgroundColor: 'lightblue',
      bottom: 0,
      height: '100%',
      left: 0,
      top: 0,
      width: `${(progress + 1) / (max + 1) * 100}%`,
    }
    return <div style={containerStyle}>
      <div style={progressStyle} />
    </div>
  }
}


class ScreenCaseTester extends React.Component {
  static propTypes = {
    caseData: PropTypes.shape({
      data: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
      dataPeriod: PropTypes.number,
      name: PropTypes.string.isRequired,
    }),
    onNext: PropTypes.func.isRequired,
    onResult: PropTypes.func.isRequired,
    translate: PropTypes.func.isRequired,
    waitingTimeBeforeTestSeconds: PropTypes.number.isRequired,
  }

  static defaultProps = {
    waitingTimeBeforeTestSeconds: 3,
  }

  state = {
    analysisIn: this.props.waitingTimeBeforeTestSeconds,
    data: this.props.caseData && this.props.caseData.data,
    dataPeriod: this.props.caseData && this.props.caseData.dataPeriod || .004,
    hasStopped: false,
    isMassageInProgress: true,
  }

  componentDidMount() {
    this.startNewCase()
  }

  componentDidUpdate(prevProps) {
    const {caseData, waitingTimeBeforeTestSeconds} = this.props
    if (prevProps.caseData !== caseData) {
      this.setState({
        analysisIn: waitingTimeBeforeTestSeconds,
        data: caseData && caseData.data,
        dataPeriod: caseData && caseData.dataPeriod || .004,
        hasStopped: false,
        isMassageInProgress: true,
      })
      this.startNewCase()
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  screenRef = React.createRef()

  startNewCase() {
    clearInterval(this.interval)
    this.interval = setInterval(() => {
      const {analysisIn} = this.state
      if (analysisIn === 1) {
        clearInterval(this.interval)
        this.handleStart()
        return
      }
      this.setState({analysisIn: analysisIn - 1})
    }, 1000)
  }

  handleStart = () => {
    this.screenRef.current && this.screenRef.current.start()
    this.setState({isMassageInProgress: false})
  }

  handleStop = willShock => () => {
    if (!this.screenRef.current) {
      return
    }
    const {onResult} = this.props
    const testTimeMillisec = this.screenRef.current.stop()
    this.setState({hasStopped: true, testTimeMillisec, willShock})
    onResult({at: new Date(), testTimeMillisec, willShock})
  }

  renderCountDown() {
    const {translate} = this.props
    const {analysisIn} = this.state
    const numberStyle = {
      fontSize: 300,
      fontWeight: 500,
      textShadow: '0 4px 24px rgba(0, 0, 0, 0.5)',
    }
    return <div style={{fontWeight: 900}}>
      {translate('Chest compressions in progress…')}<br />
      {translate('Analyzing in:')}
      <div style={numberStyle}>
        {analysisIn}
      </div>
    </div>
  }

  renderButtons() {
    const {onNext, translate} = this.props
    const {hasStopped, isMassageInProgress, testTimeMillisec, willShock} = this.state
    if (isMassageInProgress) {
      return null
    }

    if (hasStopped) {
      return <div style={{color: '#fff', fontSize: 16, fontWeight: 900, textAlign: 'center'}}>
        {willShock ?
          translate('You have decided to shock in') :
          translate('You have decided not to shock in')}
        <div style={{fontSize: 30, fontWeight: 500, marginBottom: 60}}>
          {(testTimeMillisec / 1000).toLocaleString(translate('locale'))} {translate('seconds')}.
        </div>
        <Button onClick={onNext}>
          {translate('Next test')}
        </Button>
      </div>
    }

    return <div style={{display: 'flex', justifyContent: 'center'}}>
      <BigButton
        onClick={this.handleStop(false)} imageSrc={massageButtonImage}
        style={{color: '#f4c814'}} alt="Massage">
        {translate('Do not shock')}
      </BigButton>
      <BigButton
        onClick={this.handleStop(true)} imageSrc={chocButtonImage}
        style={{color: '#e5533e', marginLeft: 30}} alt="Choc">
        {translate('Clear!')}<br />{translate("I'm shocking")}
      </BigButton>
    </div>
  }

  render() {
    const {data, dataPeriod = .004, isMassageInProgress} = this.state
    const overlayStyle = {
      alignItems: 'center',
      bottom: 0,
      color: '#fff',
      display: 'flex',
      justifyContent: 'center',
      left: 0,
      position: 'absolute',
      right: 0,
      top: 0,
    }
    return <div style={{margin: 'auto', maxWidth: 1000, textAlign: 'center'}}>
      <div style={{height: 500, position: 'relative'}}>
        <Screen
          width={1000} height={500} ref={this.screenRef} data={data} dataPeriod={dataPeriod}
          backgroundColor="#030d03" color="#08bf08" />
        {isMassageInProgress ? <div style={overlayStyle}>
          {this.renderCountDown()}
        </div> : null}
      </div>
      <div style={{backgroundColor: '#030d03', height: 200}}>
        {this.renderButtons()}
      </div>
    </div>
  }
}


class ScreenCasesTester extends React.Component {
  static propTypes = {
    cases: PropTypes.arrayOf(PropTypes.shape({
      data: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
      dataPeriod: PropTypes.number,
      name: PropTypes.string.isRequired,
    }).isRequired).isRequired,
    isDatasetNameShown: PropTypes.bool,
    message: PropTypes.node,
    onDone: PropTypes.func.isRequired,
    onResult: PropTypes.func,
    translate: PropTypes.func.isRequired,
  }

  state = {
    caseIndex: 0,
    isMessageHidden: false,
  }

  componentDidUpdate(prevProps) {
    if (prevProps.cases !== this.props.cases) {
      this.setState({caseIndex: 0, isMessageHidden: false})
    }
  }

  handleNext = () => {
    const {cases, onDone} = this.props
    const {caseIndex} = this.state
    if (caseIndex + 1 >= cases.length) {
      onDone()
    }
    this.setState({caseIndex: caseIndex + 1})
  }

  handleResult = result => {
    const {cases, onResult} = this.props
    onResult && onResult(cases[this.state.caseIndex], result)
  }

  hideMessage = () => {
    this.setState({isMessageHidden: true})
  }

  render() {
    const {cases, isDatasetNameShown, message, translate} = this.props
    const {caseIndex, isMessageHidden} = this.state
    if (message && !caseIndex && !isMessageHidden) {
      const messageContainerStyle = {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
        height: 600,
        justifyContent: 'center',
      }
      return <div style={messageContainerStyle}>
        {message}
        <Button onClick={this.hideMessage} style={{marginTop: 40}}>
          {translate("Let's go!")}
        </Button>
      </div>
    }
    if (caseIndex >= cases.length) {
      return null
    }
    const nameStyle = {
      textAlign: 'center',
    }
    return <React.Fragment>
      <ScreenCaseTester
        caseData={cases[caseIndex]} onResult={this.handleResult} onNext={this.handleNext}
        translate={translate} />
      <ProgressBar max={cases.length} progress={caseIndex} />
      {isDatasetNameShown ? <div style={nameStyle}>{cases[caseIndex].name}</div> : null}
      {/* TODO(pascal): Reenable after Feb 2020. */}
      {/* <Partners /> */}
    </React.Fragment>
  }
}


function createFirestore() {
  const app = initializeApp({
    apiKey: 'AIzaSyAAj4_PVrwNFerNsPYjoUnhTy3W5SFnd-8',
    authDomain: 'simul-choc.firebaseapp.com',
    databaseURL: 'https://simul-choc.firebaseio.com',
    messagingSenderId: '698161414648',
    projectId: 'simul-choc',
    storageBucket: 'simul-choc.appspot.com',
  })
  const db = getFirestore(app)
  return db
}


const Sites = {
  IADE: {
    lang: 'fr',
    title: 'IADE',
  },
  expert: {
    lang: 'fr',
    title: 'Expert·e',
  },
  'medecine-ge': {
    lang: 'fr',
  },
  public: {
    isAnonymous: true,
    lang: 'en',
  },
  rea: {
    lang: 'fr',
    title: 'Anesthésie/Réanimation',
  },
}


class AppBase extends React.Component {
  static propTypes = {
    firestore: PropTypes.object.isRequired,
  }

  static getKind({hostname, pathname}) {
    if (hostname.match(/simul-shock/) || pathname.match(/^\/public(\/|$)/i)) {
      return 'public'
    }
    if (pathname.match(/^\/expert(\/|$)/i)) {
      return 'expert'
    }
    if (pathname.match(/^\/IADE(\/|$)/i)) {
      return 'IADE'
    }
    if (pathname.match(/^\/rea(\/|$)/i)) {
      return 'rea'
    }
    if (pathname.match(/^\/medecine-ge(\/|$)/i)) {
      return 'medecine-ge'
    }
  }

  static getRestrictCases(search) {
    const {restrict, random} = parse(search)
    if (restrict && (typeof restrict === 'string')) {
      const isInRestricted = new Set(restrict.split(','))
      const restrictedCases =
        allCases.filter(({name}) => isInRestricted.has(name.substr('patient '.length)))
      return restrictedCases.length ? restrictedCases : null
    }
    if (random && typeof random === 'string') {
      const randomCount = parseInt(random, 10)
      return allCases.slice(0, randomCount)
    }
  }

  static getTesterId(search) {
    const {id} = parse(search)
    if (!id || (typeof id !== 'string')) {
      return null
    }
    return id
  }

  state = {
    blockIndex: 0,
    firestoreId: '',
    formValues: null,
    isLandingPageShown: true,
    kind: AppBase.getKind(window.location),
    restrictedCases: AppBase.getRestrictCases(window.location.search),
    userChoices: [],
  }

  componentDidMount() {
    const {firestore} = this.props
    const testerId = Cookies.get('testerId') || AppBase.getTesterId(window.location.search)
    if (testerId) {
      getDoc(doc(firestore, 'testers', testerId)).then(doc => {
        if (!doc.exists()) {
          return
        }
        const {userChoices = [], ...formValues} = doc.data()
        const hasFinished = userChoices.length > 50
        this.setState({
          blockIndex: hasFinished ? 3 : 0,
          firestoreId: testerId,
          formValues,
          isLandingPageShown: !hasFinished,
          userChoices,
        })
      })
    }
  }

  getSiteProps() {
    return Sites[this.state.kind] || {}
  }

  handleFormComplete = formValues => {
    if (!this.getSiteProps().isAnonymous) {
      const {firestore} = this.props
      const doc = {registeredAt: new Date()}
      for (const key in formValues) {
        const value = formValues[key]
        if (value !== undefined) {
          doc[key] = value
        }
      }
      if (this.state.kind) {
        doc['userType'] = this.state.kind
      }
      addDoc(collection(firestore, 'testers'), doc).then(docRef => {
        Cookies.set('testerId', docRef.id)
        this.setState({firestoreId: docRef.id})
      })
    }
    this.setState({formValues})
  }

  handleResult = ({name}, choice) => {
    const {firestore} = this.props
    const {firestoreId, userChoices: previousUserChoices} = this.state
    const userChoices = previousUserChoices.concat([{name, ...choice}])
    if (firestoreId) {
      updateDoc(doc(firestore, 'testers', firestoreId), {userChoices})
    }
    this.setState({userChoices})
  }

  handleNextBlock = () => {
    const {blockIndex} = this.state
    this.setState({blockIndex: blockIndex + 1})
  }

  hideLandingPage = () => {
    this.setState({isLandingPageShown: false})
  }

  renderDone() {
    const {lang} = this.getSiteProps()
    const {formValues, userChoices} = this.state
    return <ResultsTable
      choices={userChoices} t={getTranslator(lang)} hasParticipated={!!formValues?.email} />
  }

  render() {
    const {blockIndex, formValues, isLandingPageShown, kind, restrictedCases, firestoreId,
      userChoices} = this.state
    const pageStyle = {
      backgroundColor: '#e7e7e7',
      boxSizing: 'border-box',
      fontFamily: 'Lato',
      left: 0,
      minHeight: '100vh',
      padding: '15px 20px',
      position: 'absolute',
      right: 0,
      top: 0,
    }

    const {isAnonymous, lang, title} = this.getSiteProps()
    const translate = getTranslator(lang)

    if (isLandingPageShown) {
      return <LandingPage
        style={pageStyle} onNext={this.hideLandingPage}
        title={title} translate={translate} />
    }

    const isExpertPage = kind === 'expert'

    if (!formValues && !(isExpertPage && restrictedCases)) {
      return <Form
        onDone={this.handleFormComplete} kind={kind} isAnonymous={isAnonymous}
        translate={translate} />
    }

    const cases = isExpertPage ? (restrictedCases || allCases) : shuffledDataBlocks[blockIndex]
    const isDone = isExpertPage ? blockIndex : !cases

    return <div style={pageStyle}>
      <div style={{marginBottom: 11}}>
        <img src={translate('logo-svg')} alt={translate('Simul Shock')} style={{height: 35}} />
        {isDone ?
          this.renderDone() :
          <ScreenCasesTester
            cases={cases}
            onResult={
              (isExpertPage && (!restrictedCases || !firestoreId) || blockIndex) ? this.handleResult : undefined
            }
            onDone={this.handleNextBlock}
            message={isExpertPage ? null :
              translate(blockMessages[blockIndex], formValues.gender)}
            isDatasetNameShown={isExpertPage && !!restrictedCases} translate={translate}/>
        }
      </div>
    </div>
  }
}
const App = () => {
  const [db] = useState(createFirestore)
  return <AppBase firestore={db} />
}


export {App}
