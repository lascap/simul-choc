import _fromPairs from 'lodash/fromPairs'
import _keyBy from 'lodash/keyBy'
import _memoize from 'lodash/memoize'
import PropTypes from 'prop-types'
import React, {useCallback, useMemo, useRef, useState} from 'react'

import allCases from './data'
import results from './data/results'
import {Screen} from './screen'

const mappedResults = _keyBy(results, 'name')


const statsGroups = [
  {
    name: 'Coarse Ventricular Fibrillation',
    patients: new Set([4, 7, 8, 66]),
  },
  {
    name: 'Fine Ventricular Fibrillation',
    patients: new Set([1, 2, 21, 55, 62]),
  },
  {
    name: 'Ventricular Tachycardia',
    patients: new Set([9, 31, 47, 48, 49, 50, 54]),
  },
]
const nameToGroup = _fromPairs(allCases.map(({name, num}) => {
  const group = statsGroups.find(({patients}) => patients.has(num))
  return [name, group ? group.name : '']
}))


const pageStyle = {
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
}
const correctStyle = {
  backgroundColor: '#dfd',
}
const wrongStyle = {
  backgroundColor: '#fdd',
}
const cellStyle = {
  padding: '10px 15px',
}
const statsDivStyle = {
  marginBottom: 20,
  position: 'relative',
}
const thankYouStyle = {
  marginBottom: 20,
}
const fixedStyle = {
  alignItems: 'center',
  display: 'flex',
  height: '100vh',
  justifyContent: 'center',
  left: 0,
  pointerEvents: 'none',
  position: 'fixed',
  top: 0,
  width: '100%',
}
const screenContainerStyle = {
  pointerEvents: 'all',
  position: 'relative',
}
const closeStyle = {
  alignItems: 'center',
  backgroundColor: '#fff',
  border: 'solid 2px #000',
  borderRadius: 20,
  cursor: 'pointer',
  display: 'flex',
  height: 20,
  justifyContent: 'center',
  position: 'absolute',
  right: -10,
  top: -10,
  width: 20,
}
const moreStatsStyle = {
  color: '#777',
  cursor: 'pointer',
  fontSize: 'smaller',
  fontStyle: 'italic',
  textAlign: 'center',
  textDecoration: 'underline',
}


const getRowStyle = _memoize((isCorrect, isSelected) => ({
  ...(isCorrect ? correctStyle : wrongStyle),
  outline: isSelected ? 'solid 1px black' : 'none',
}), (isCorrect, isSelected) => `${!!isCorrect}-${!!isSelected}`)


const LinkToPatientBase = ({children, onClick, name}) => {
  const clickHandler = useCallback(() => onClick(name), [onClick, name])
  return <a onClick={clickHandler} href={`#${name}`}>
    {children}
  </a>
}
LinkToPatientBase.propTypes = {
  children: PropTypes.node,
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}
const LinkToPatient = React.memo(LinkToPatientBase)


const StatsBase = ({patients, t, title}) => {
  const sensitivity = useMemo(
    () =>
      patients.filter(({name, willShock}) =>
        willShock && mappedResults[name] && mappedResults[name].result === 'SHOCK').length /
      patients.filter(({name}) =>
        mappedResults[name] && mappedResults[name].result === 'SHOCK').length,
    [patients],
  )
  const specificity = useMemo(
    () =>
      patients.filter(({name, willShock}) =>
        !willShock && mappedResults[name] && mappedResults[name].result === 'NO SHOCK').length /
      patients.filter(({name}) =>
        mappedResults[name] && mappedResults[name].result === 'NO SHOCK').length * 1,
    [patients],
  )
  return <div>
    {title ? <div><strong>{title}</strong></div> : null}
    {isNaN(sensitivity) ? null : <div>
      {t('Sensitivity:')} {(Math.round(sensitivity * 100) / 100).toLocaleString(t('locale'))}
    </div>}
    {isNaN(specificity) ? null : <div>
      {t('Specificity:')} {(Math.round(specificity * 100) / 100).toLocaleString(t('locale'))}
    </div>}
  </div>
}
StatsBase.propTypes = {
  patients: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    willShock: PropTypes.bool.isRequired,
  }).isRequired),
  t: PropTypes.func.isRequired,
  title: PropTypes.node,
}
const Stats = React.memo(StatsBase)


const ResultsTable = ({choices, hasParticipated, t}) => {
  const [patientSelected, selectPatient] = useState(undefined)
  const clearPatient = useCallback(() => selectPatient())
  const [isStatsTipShown, setStatsTipShown] = useState(false)
  const showStatsTip = useCallback(() => setStatsTipShown(true))
  const hideStatsTip = useCallback(() => setStatsTipShown(false))
  const [isMoreStatsShown, setMoreStatsShown] = useState(false)
  const showMoreStats = useCallback(() => setMoreStatsShown(true))
  const hideMoreStats = useCallback(() => setMoreStatsShown(false))
  const [data, dataPeriod] = useMemo(() => {
    const selectedCase = patientSelected && allCases.find(({name}) => name === patientSelected)
    if (selectedCase) {
      return [selectedCase.data, selectedCase.dataPeriod || .004]
    }
    return [undefined, undefined]
  }, [patientSelected])
  const screenRef = useRef()
  const selectPatientAndStart = useCallback((patient) => {
    selectPatient(patient)
    if (patient) {
      setTimeout(() => screenRef.current && screenRef.current.start(), 2000)
    }
  }, [screenRef])
  const statsTipStyle = useMemo(() => ({
    backgroundColor: '#fff',
    borderRadius: 10,
    left: '50%',
    opacity: isStatsTipShown ? 1 : 0,
    padding: 15,
    pointerEvents: 'none',
    position: 'absolute',
    top: '100%',
    transform: 'translate(-50%, 10px)',
    transition: '450ms',
    width: 550,
  }), [isStatsTipShown])

  const patientsIndex = new Map()
  choices.forEach(({name}, index) => {
    patientsIndex[name] = index
  })
  const uniqueChoices = choices.filter(({name}, index) => patientsIndex[name] === index)

  return <div style={pageStyle}>
    <h1>{t('Your Results')}</h1>
    {hasParticipated ? <div style={thankYouStyle}>
      {t('Thank you for participating')}
    </div> : null}
    <div style={statsDivStyle} onMouseEnter={showStatsTip} onMouseLeave={hideStatsTip}>
      <Stats patients={uniqueChoices} t={t} title={isMoreStatsShown ? 'Global' : undefined} />
      {isMoreStatsShown ? <React.Fragment>
        {statsGroups.map(({name}) => <Stats
          key={`stats-${name}`} title={t(name)} t={t}
          patients={uniqueChoices.filter(({name: patientName}) =>
            nameToGroup[patientName] === name)} />)}
        <div role="button" onClick={hideMoreStats} style={moreStatsStyle}>
          {t('show less stats')}
        </div>
      </React.Fragment> : <div role="button" onClick={showMoreStats} style={moreStatsStyle}>
        {t('show more stats…')}
      </div>}
      <div style={statsTipStyle}>
        {t('True Positive (TP): Experts and you said “shock”')}<br />
        {t('True Negative (TN): Experts and you said “no shock”')}<br />
        {t('False Positive (FP): Experts said “no shock” and you said “shock”')}<br />
        {t('False Negative (FN): Experts said “shock” and you said “no shock”')}<br />
        {t('Sensitivity = TP/(TP+FN)')}<br />
        {t('Specificity = TN/(TN+FP)')}
      </div>
    </div>
    {data && dataPeriod ? <div style={fixedStyle}>
      <div style={screenContainerStyle}>
        <Screen
          width={1000} height={500} data={data} dataPeriod={dataPeriod}
          backgroundColor="#030d03" color="#08bf08" ref={screenRef} />
        <div style={closeStyle} onClick={clearPatient}>X</div>
      </div>
    </div> : null}
    <table>
      <thead><tr>
        <th>{t('Patient')}</th>
        <th>{t("Expert's decision")}</th>
        <th>{t('Your decision')}</th>
        <th>{t('Your time')}</th>
        <th />
        <th>{t('Type')}</th>
      </tr></thead>
      <tbody>
        {uniqueChoices.filter(({name}) => !!mappedResults[name]).map(choice => {
          const {name, testTimeMillisec, willShock} = choice
          const result = willShock ? 'SHOCK' : 'NO SHOCK'
          const isCorrect = result === mappedResults[name].result
          return <tr key={name} style={getRowStyle(isCorrect, name === patientSelected)}>
            <td style={cellStyle}>{name}</td>
            <td style={cellStyle}>{t(mappedResults[name].result)}</td>
            <td style={cellStyle}>{t(result)}</td>
            <td style={cellStyle}>{(testTimeMillisec / 1000).toLocaleString(t('locale'))}s</td>
            <td style={cellStyle}><LinkToPatient onClick={selectPatientAndStart} name={name}>
              {t('see it again')}
            </LinkToPatient></td>
            <td style={cellStyle}>{t(nameToGroup[name])}</td>
          </tr>
        })}
      </tbody>
    </table>
  </div>
}
ResultsTable.propTypes = {
  choices: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired).isRequired,
  hasParticipated: PropTypes.bool,
  t: PropTypes.func.isRequired,
}


export {ResultsTable}
