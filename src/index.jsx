import ReactDOM from 'react-dom'
import React from 'react'
import {App} from './app'

const rootElement = document.getElementById('app')
if (rootElement) {
	ReactDOM.render(
		<React.StrictMode>
			<App />
		</React.StrictMode>,
        rootElement
        )
} else {
	// eslint-disable-next-line no-console
	console.error('DOM is missing an element with "root" id.')
}
