import _memoize from 'lodash/memoize'
import logoEnImage from './images/logo-en.svg'
import logoImage from './images/logo.svg'


/* eslint max-len: off */
const LOCALES = {
  en: {
    instructions: 'You will be confronted successively with the electrical rhythms of several patients in cardiac arrest, and you will have to make a choice: immediate delivery of an electrical shock or no shock.',
    // TODO(pascal): Check why we do not show those in English.
    'instructions/last': '',
    'instructions/last-for-expert': '',
    'instructions/no-pulse': 'We ask you to consider that whatever the proposed rhythm, the patient is indeed a patient in clinical cardiac arrest, therefore unconscious and without a palpable pulse.',
    intro: 'During the computer simulation that follows, you perform specialized resuscitation of a cardiac arrest with a manual defibrillator.',
    locale: 'en',
    'logo-svg': logoEnImage,
  },
  fr: {
    'Analyzing in:': 'Analyse dans\u00A0:',
    'Are you ready? The simulation for the first 30 patients in cardiac arrest is waiting for your expertise.': 'Vous êtes prêt·e\u00A0? La simulation commence maintenant par une trentaine de tracés.',
    'Are you ready? The simulation for the first 30 patients in cardiac arrest is waiting for your expertise._female': 'Vous êtes prête\u00A0? La simulation commence maintenant par une trentaine de tracés.',
    'Are you ready? The simulation for the first 30 patients in cardiac arrest is waiting for your expertise._male': 'Vous êtes prêt\u00A0? La simulation commence maintenant par une trentaine de tracés.',
    'Chest compressions in progress…': 'Massage en cours',
    'Clear!': 'Écartez vous,',
    'Coarse Ventricular Fibrillation': 'Fibrillation ventriculaire grossière',
    'Do not shock': 'Pas de choc',
    'Each analysis must be considered totally independent of the previous ones (different patients).': 'Chaque analyse doit être considérée totalement indépendante des précédentes.',
    "Expert's decision": "Décision de l'expert",
    'False Negative (FN): Experts said “shock” and you said “no shock”': 'Faux Négatif (FN)\u00A0: les experts ont choisi «\u00A0choc\u00A0» et vous «\u00A0pas de choc\u00A0»',
    'False Positive (FP): Experts said “no shock” and you said “shock”': 'Faux Positif (FP)\u00A0: les experts ont choisi «\u00A0pas de choc\u00A0» et vous «\u00A0choc\u00A0»',
    'Fine Ventricular Fibrillation': 'Fibrillation ventriculaire fine',
    'First of all, 3 trails to get acquainted with the simulator.': "Tout d'abord 3 tracés pour vous familiariser avec le simulateur.",
    "I'm shocking": 'je choque',
    "Let's go!": "C'est parti\u00A0!",
    'NO SHOCK': 'PAS DE CHOC',
    'Next test': 'Test suivant',
    'SHOCK': 'CHOC',
    'Sensitivity = TP/(TP+FN)': 'Sensibilité = VP/(VP+FN)',
    'Sensitivity:': 'Sensibilité\u00A0:',
    'Simul Shock': 'Simul Choc',
    'Specificity = TN/(TN+FP)': 'Spécificité = VN/(VN+FP)',
    'Specificity:': 'Spécificité\u00A0:',
    'Start': 'Commencer',
    'Thank you for participating': 'Merci pour votre participation',
    'The different rhythms that will follow each other are randomly proposed from a database.': "Les différents rythmes qui s'enchaineront sont proposés de manière aléatoire dans une base de données.",
    'Tired? Think it could be 4am after a sleepless night! Blow a few moments and here we go again! Your results are waiting for you at the end.': 'Reposez-vous un peu, et quand vous êtes prêt·e, la deuxième moitié des tracés vous attend.',
    'Tired? Think it could be 4am after a sleepless night! Blow a few moments and here we go again! Your results are waiting for you at the end._female': 'Reposez-vous un peu, et quand vous êtes prête, la deuxième moitié des tracés vous attend.',
    'Tired? Think it could be 4am after a sleepless night! Blow a few moments and here we go again! Your results are waiting for you at the end._male': 'Reposez-vous un peu, et quand vous êtes prêt, la deuxième moitié des tracés vous attend.',
    'True Negative (TN): Experts and you said “no shock”': 'Vrai Négatif (VN)\u00A0: les experts and vous avez choisi «\u00A0pas de choc\u00A0»',
    'True Positive (TP): Experts and you said “shock”': 'Vrai Positif (VP)\u00A0: les experts et vous avez choisi «\u00A0choc\u00A0»',
    'Ventricular Tachycardia': 'Tachycardie ventriculaire',
    'You have decided not to shock in': 'Vous avez décidé de ne pas choquer en',
    'You have decided to shock in': 'Vous avez décidé de choquer en',
    'Your Results': 'Vos résultats',
    'Your decision': 'Votre décision',
    'Your reaction time is also collected.': 'Votre temps de réaction est également recueilli.',
    'Your time': 'Votre temps',
    instructions: "Vous allez être confronté·e successivement à des rythmes électriques d'un·e patient·e, et vous devrez faire un choix\u00A0: délivrance immédiate d'un choc électrique ou bien pas de choc.",
    'instructions/last': "Vos réponses vont être comparées à celles prises par le DSA lors de l'intervention d'où le tracé provient, ainsi qu'aux réponses de deux médecins expert·e·s.",
    'instructions/last-for-expert': "Vos réponses vont être comparées à celles prises par le DSA lors de l'intervention d'où le tracé provient, ainsi qu'aux réponses de deux autres médecins expert·e·s.",
    // TODO(pascal): Check with Clément why we don't have this one in French.
    'instructions/no-pulse': '',
    intro: "Lors de la simulation informatique qui va suivre, vous réalisez une réanimation spécialisée d'arrêt cardiaque avec un défibrillateur manuel.",
    locale: 'fr',
    'logo-svg': logoImage,
    seconds: 'secondes',
    'see it again': 'revoir le tracé',
    'show less stats': 'voir moins de stats',
    'show more stats…': 'voir plus de stats…',
  },
}


let selectedLang = LOCALES.fr


function getTranslatorNotCached(lang) {
  selectedLang = LOCALES[lang || 'fr'] || LOCALES.fr
  return (text, context) => {
    if (context) {
      const textWithContext = selectedLang[text + '_' + context]
      if (typeof textWithContext === 'string') {
        return textWithContext
      }
    }
    const selectedText = selectedLang[text]
    if (typeof selectedText === 'string') {
      return selectedText
    }
    return text
  }
}
const getTranslator = _memoize(getTranslatorNotCached)


export {getTranslator}
