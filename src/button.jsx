import PropTypes from 'prop-types'
import Radium from 'radium'
import React from 'react'


class ButtonBase extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    isInput: PropTypes.bool,
    style: PropTypes.object,
  }

  render() {
    const {children, isInput, style, ...otherProps} = this.props
    const buttonStyle = {
      ':hover': {
        backgroundColor: '#61b0f1',
      },
      backgroundColor: '#4190e1',
      border: 'none',
      borderRadius: 6,
      color: '#fff',
      cursor: 'pointer',
      fontFamily: 'inherit',
      fontSize: 15,
      fontWeight: 900,
      padding: '14px 34px',
      transition: 'background-color .5s',
      ...style,
    }
    if (isInput) {
      return <input style={buttonStyle} {...otherProps}>
        {children}
      </input>
    }
    return <button style={buttonStyle} {...otherProps}>
      {children}
    </button>
  }
}
const Button = Radium(ButtonBase)


class BigButton extends React.Component {
  static propTypes = {
    alt: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    imageSrc: PropTypes.string.isRequired,
    style: PropTypes.object,
  }

  state = {
    isHovered: false,
  }

  render() {
    const {alt, children, imageSrc, style, ...otherProps} = this.props
    const {isHovered} = this.state
    const containerStyle = {
      cursor: 'pointer',
      position: 'relative',
      ...style,
    }
    const childrenStyle = {
      bottom: 156,
      fontSize: 20,
      fontWeight: 500,
      left: 0,
      opacity: isHovered ? 1 : 0,
      position: 'absolute',
      textAlign: 'center',
      transition: 'opacity .1s',
      width: 176,
    }
    return <div
      style={containerStyle} {...otherProps}
      onMouseEnter={() => this.setState({isHovered: true})}
      onMouseLeave={() => this.setState({isHovered: false})}>
      <img src={imageSrc} alt={alt} style={{height: 76, padding: 50}} />
      <div style={childrenStyle}>
        {children}
      </div>
    </div>
  }
}


export {BigButton, Button}
