import _memoize from 'lodash/memoize'
import _pick from 'lodash/pick'
import PropTypes from 'prop-types'
import React from 'react'

import {Button} from './button'


class ProfileForm extends React.Component {
  static propTypes = {
    onDone: PropTypes.func.isRequired,
  }

  state = {}

  ageRef = React.createRef()

  genderRef = React.createRef()

  getGenderChangeHandler = _memoize(gender => () => {
    this.setState({gender})
  })

  handleChangeAge = ({target: {value: age}}) => this.setState({age})

  handleSubmit = event => {
    event.preventDefault()

    const {onDone} = this.props
    const {age, gender} = this.state
    if (!gender) {
      this.genderRef.current && this.genderRef.current.focus()
      return
    }
    const ageInt = parseInt((age || '').trim(), 10)
    if (!ageInt) {
      this.ageRef.current && this.ageRef.current.focus()
      return
    }
    onDone({age: ageInt, gender})
  }

  render() {
    const {age, gender} = this.state
    return <form onSubmit={this.handleSubmit} style={{padding: '0 20px'}}>
      <h2 style={{textAlign: 'center'}}>Profil</h2>

      <div style={{marginTop: 25}}>
        <strong style={{display: 'block', marginBottom: 10}}>Vous êtes&nbsp;:</strong>
        <div style={{display: 'flex', justifyContent: 'space-around'}}>
          <label style={{alignItems: 'center', display: 'flex'}}>
            <input
              type="radio" name="gender"
              checked={gender === 'male'}
              ref={this.genderRef}
              onChange={this.getGenderChangeHandler('male')} />
              un homme
          </label>

          <label style={{alignItems: 'center', display: 'flex'}}>
            <input
              type="radio" name="gender"
              checked={gender === 'female'}
              onChange={this.getGenderChangeHandler('female')} />
            une femme
          </label>
        </div>
      </div>

      {gender ? <div style={{marginTop: 25}}>
        <strong style={{display: 'block', marginBottom: 10}}>Quel âge avez vous&nbsp;?</strong>
        <input
          type="number" value={age || ''}
          onChange={this.handleChangeAge}
          ref={this.ageRef} />
      </div> : null}

      {age && gender ? <Button isInput={true} type="submit" style={{marginTop: 20}} /> : null}
    </form>
  }
}


class RadioButtonQuestion extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    customName: PropTypes.node,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.node.isRequired,
      value: PropTypes.string.isRequired,
    }).isRequired).isRequired,
    style: PropTypes.object,
    value: PropTypes.string,
  }

  static getCustomValue({options, value}) {
    if (options.find(({value: optionValue}) => value === optionValue)) {
      return ''
    }
    return value
  }

  state = {
    customValue: RadioButtonQuestion.getCustomValue(this.props),
    isCustom: !!RadioButtonQuestion.getCustomValue(this.props),
  }

  radioRef = React.createRef()

  customRef = React.createRef()

  focus() {
    const ref = this.state.isCustom ? this.customRef : this.radioRef
    ref.current && ref.current.focus()
  }

  getRadioChangeHandler = _memoize(value => () => {
    this.setState({isCustom: false})
    this.props.onChange(value)
  })

  handleSelectCustom = () => {
    this.setState({isCustom: true})
    this.props.onChange(this.state.customValue)
  }

  handleCustomChange = event => {
    const customValue = event.target.value.trim()
    this.setState({customValue})
    this.props.onChange(customValue)
  }

  render() {
    const {children, customName, name, options, style, value} = this.props
    const {customValue, isCustom} = this.state
    return <div style={style}>
      <strong style={{display: 'block', marginBottom: 10}}>
        {children}
      </strong>
      {options.map(({name: optionName, value: optionValue}, index) => <label
        ref={index ? undefined : this.radioRef}
        checked={!isCustom && value === optionValue}
        onChange={this.getRadioChangeHandler(optionValue)}
        key={optionValue} style={{display: 'flex', padding: '5px 0'}}>
        <input type="radio" value={optionValue} name={name} />
        {optionName}
      </label>)}
      {customName ? <label style={{display: 'block'}}>
        <input
          type="radio" name={name} checked={isCustom}
          onChange={this.handleSelectCustom} />
        {customName}&nbsp;: <input
          type="text" value={customValue || ''} ref={this.customRef}
          onFocus={this.handleSelectCustom}
          onChange={this.handleCustomChange} />
      </label> : null}
    </div>
  }
}


class CheckboxesQuestion extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    customName: PropTypes.node,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.node.isRequired,
      value: PropTypes.string.isRequired,
    }).isRequired).isRequired,
    style: PropTypes.object,
    values: PropTypes.arrayOf(PropTypes.string),
  }

  static splitCustomValues({options, values}) {
    const optionsAsSet = new Set()
    options.forEach(({value}) => optionsAsSet.add(value))
    const checkedValues = new Set()
    const customValues = [];
    (values || []).forEach(value => {
      if (checkedValues.has(value)) {
        return
      }
      checkedValues.add(value)
      if (!optionsAsSet.has(value)) {
        customValues.push(value)
      }
    })
    return {
      checkedValues,
      customValue: customValues.join(','),
    }
  }

  static getInitialState(props) {
    const {customValue} = CheckboxesQuestion.splitCustomValues(props)
    return {
      customValue,
      isCustomSelected: !!customValue,
    }
  }

  state = CheckboxesQuestion.getInitialState(this.props)

  firstCheckboxRef = React.createRef()

  focus() {
    const ref = this.firstCheckboxRef
    ref.current && ref.current.focus()
  }

  getCheckChangeHandler = _memoize(value => () => {
    const {onChange, options, values} = this.props
    const valuesAsSet = new Set();
    (values || []).forEach(oldValue => valuesAsSet.add(oldValue))
    if (valuesAsSet.has(value)) {
      valuesAsSet.delete(value)
    } else {
      valuesAsSet.add(value)
    }
    const newValues = options.map(({value: optionValue}) => optionValue).
      filter(optionValue => valuesAsSet.has(optionValue))
    const {customValue, isCustomSelected} = this.state
    if (isCustomSelected && customValue) {
      newValues.push(customValue)
    }
    onChange(newValues)
  })

  handleToggleCustom = () => {
    const {customValue, isCustomSelected: wasCustomSelected} = this.state
    this.setState({isCustomSelected: !wasCustomSelected})
    if (!customValue) {
      return
    }
    const {onChange, options, values} = this.props
    if (!wasCustomSelected) {
      onChange(values.concat(customValue.split(',')))
      return
    }
    const valuesAsSet = new Set()
    values.forEach(value => valuesAsSet.add(value))
    onChange(options.map(({value: optionValue}) => optionValue).
      filter(value => valuesAsSet.has(value)))
  }

  handleSelectCustom = () => {
    if (!this.state.isCustomSelected) {
      this.handleToggleCustom()
    }
  }

  handleCustomChange = event => {
    const customValue = event.target.value.trim()
    const {customValue: previousCustomValue} = this.state
    if (customValue === previousCustomValue) {
      return
    }
    const {onChange, options, values} = this.props
    this.setState({customValue})
    const optionsAsSet = new Set()
    options.forEach(({value}) => optionsAsSet.add(value))
    onChange(values.filter(value => optionsAsSet.has(value)).concat([customValue]))
  }

  render() {
    const {children, customName, options, style} = this.props
    const {customValue, isCustomSelected} = this.state
    const {checkedValues} = CheckboxesQuestion.splitCustomValues(this.props)
    return <div style={style}>
      <strong style={{display: 'block', marginBottom: 10}}>
        {children}
      </strong>
      {options.map(({name: optionName, value: optionValue}, index) => <label
        ref={index ? undefined : this.firstCheckboxRef}
        checked={checkedValues.has(optionValue)}
        onChange={this.getCheckChangeHandler(optionValue)}
        key={optionValue} style={{display: 'flex', padding: '5px 0'}}>
        <input type="checkbox" value={optionValue} />
        {optionName}
      </label>)}
      {customName ? <label style={{display: 'block'}}>
        <input
          type="checkbox" checked={isCustomSelected}
          onChange={this.handleToggleCustom} />
        {customName}&nbsp;: <input
          type="text" value={customValue || ''}
          onFocus={this.handleSelectCustom}
          onChange={this.handleCustomChange} />
      </label> : null}
    </div>
  }
}


class NumberInput extends React.Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.number,
  }

  ref = React.createRef()

  focus() {
    this.ref.current && this.ref.current.focus()
  }

  handleChange = event => {
    const value = parseInt(event.target.value.trim(), 10)
    this.props.onChange(value || value === 0 ? value : undefined)
  }

  render() {
    const {value} = this.props
    return <input
      {...this.props} type="number" onChange={this.handleChange} ref={this.ref}
      value={value || value === 0 ? value : ''} />
  }
}


class ReanimateForm extends React.Component {
  static propTypes = {
    gender: PropTypes.oneOf(['female', 'male']),
    kind: PropTypes.oneOf(['IADE', 'medecine-ge', 'rea']),
    onDone: PropTypes.func.isRequired,
  }

  state = {}

  getFieldChangeHandler = _memoize(field => value => this.setState({[field]: value}))

  getInputFieldChangeHandler = _memoize(field => event =>
    this.setState({[field]: event.target.value}))

  getArrayFieldChangeHandler = _memoize(field => values =>
    this.setState({[field]: values.join(',')}))

  inputsRef = {
    autonomousYears: React.createRef(),
    experienceYears: React.createRef(),
    handleRealCA: React.createRef(),
    handleRealCAAs: React.createRef(),
    handleSimulCA: React.createRef(),
    iadeYears: React.createRef(),
    meanRea: React.createRef(),
    numOnCallsPerMonth: React.createRef(),
    numRea: React.createRef(),
    onCallLastYear: React.createRef(),
    preciseService: React.createRef(),
    preferredMultiMode: React.createRef(),
    seaRescue: React.createRef(),
    service: React.createRef(),
    training: React.createRef(),
  }

  focusOnRef = ref => ref.current && ref.current.focus()

  handleSubmit = event => {
    event.preventDefault()

    const {kind, onDone} = this.props
    const requiredFields = kind === 'IADE' ?
      ['service', 'iadeYears', 'numRea', 'meanRea'] :
      kind === 'rea' ?
        ['doctor', 'service', 'experienceYears', 'numRea', 'meanRea'] :
        kind === 'medecine-ge' ?
          ['experienceYears', 'service', 'training', 'onCallLastYear',
            'handleRealCA', 'handleSimulCA', 'meanRea', 'preferredMultiMode'] :
          ['autonomousYears', 'numRea', 'meanRea']
    const optionalFields = []
    if (kind === 'medecine-ge' && this.state.service === 'civil') {
      requiredFields.push('preciseService')
    }
    if (kind === 'medecine-ge' && this.state.service === 'militaire') {
      requiredFields.push('seaRescue')
    }
    if (kind === 'medecine-ge' && this.state.onCallLastYear !== 'non') {
      requiredFields.push('numOnCallsPerMonth')
    }
    if (this.state.handleRealCA === 'true') {
      requiredFields.push('handleRealCAAs', 'numRea')
    }
    if (this.state.preferredMultiMode) {
      optionalFields.push('preferredMultiModeWhy')
    }
    const finalState = {}
    for (let i = 0; i < requiredFields.length; ++i) {
      const field = requiredFields[i]
      if (!this.state[field] && this.state[field] !== 0) {
        this.focusOnRef(this.inputsRef[field])
        return
      }
      finalState[field] = this.state[field]
    }
    optionalFields.forEach(field => {
      if (this.state[field]) {
        finalState[field] = this.state[field]
      }
    })
    onDone(finalState)
  }

  render() {
    const {kind, gender} = this.props
    const {autonomousYears, doctor, experienceYears, handleRealCA, handleRealCAAs,
      handleSimulCA, iadeYears, meanRea, numOnCallsPerMonth, numRea, onCallLastYear,
      preciseService, preferredMultiMode, preferredMultiModeWhy, seaRescue, service, training,
    } = this.state
    return <form onSubmit={this.handleSubmit} style={{padding: '0 20px'}}>
      <h2 style={{textAlign: 'center'}}>Réanimation</h2>

      {kind === 'IADE' ? <React.Fragment>
        <RadioButtonQuestion
          style={{marginTop: 25}} name="service" options={[
            {name: 'bloc opératoire', value: 'bloc opératoire'},
            {name: 'préhospitalier', value: 'préhospitalier'},
          ]} value={service} onChange={this.getFieldChangeHandler('service')}
          ref={this.inputsRef.service}>
          Vous exercez en priorité dans un service&nbsp;:
        </RadioButtonQuestion>

        <div style={{marginTop: 25}}>
          <strong style={{display: 'block', marginBottom: 10}}>
            Depuis combien d'années avez-vous obtenu votre diplôme d'IADE&nbsp;?
          </strong>
          <NumberInput
            value={iadeYears} ref={this.inputsRef.iadeYears}
            onChange={this.getFieldChangeHandler('iadeYears')} />
        </div>
      </React.Fragment> : null}

      {kind === 'rea' ? <React.Fragment>
        <RadioButtonQuestion
          style={{marginTop: 25}} name="doctor" options={[
            {name: 'Médecin senior', value: 'senior'},
            {name: 'Interne DESAR', value: 'interne-DESAR'},
            {name: 'Interne DESMIR', value: 'interne-DESMIR'},
          ]} value={doctor} onChange={this.getFieldChangeHandler('doctor')}
          ref={this.inputsRef.doctor}>
          Vous êtes&nbsp;:
        </RadioButtonQuestion>

        <RadioButtonQuestion
          style={{marginTop: 25}} name="service" options={[
            {name: 'bloc opératoire', value: 'bloc opératoire'},
            {name: 'réanimation', value: 'réanimation'},
          ]} value={service} onChange={this.getFieldChangeHandler('service')}
          ref={this.inputsRef.service} customName="autre">
          Vous exercez en priorité dans un service&nbsp;:
        </RadioButtonQuestion>

        <div style={{marginTop: 25}}>
          <strong style={{display: 'block', marginBottom: 10}}>
            Depuis combien d'années
            {doctor !== 'senior' ? ' avez-vous commencé votre internat' : ' exercez-vous'}&nbsp;?
          </strong>
          <NumberInput
            value={experienceYears} ref={this.inputsRef.experienceYears}
            onChange={this.getFieldChangeHandler('experienceYears')} />
        </div>
      </React.Fragment> : null}

      {kind === 'medecine-ge' ? <React.Fragment>
        <div style={{marginTop: 25}}>
          <strong style={{display: 'block', marginBottom: 10}}>
            Depuis combien d'années êtes-vous
            thésé{gender === 'female' ? 'e ' : gender === 'male' ? ' ' : '·e '}&nbsp;?
          </strong>
          <NumberInput
            value={experienceYears} ref={this.inputsRef.experienceYears}
            onChange={this.getFieldChangeHandler('experienceYears')} />
        </div>

        <RadioButtonQuestion
          style={{marginTop: 25}} name="service" options={[
            {name: 'militaire', value: 'militaire'},
            {name: 'civil', value: 'civil'},
          ]} value={service} onChange={this.getFieldChangeHandler('service')}
          ref={this.inputsRef.service}>
          Vous êtes&nbsp;:
        </RadioButtonQuestion>
        {service === 'civil' ? <div>
          <strong>Votre département d'exercice&nbsp;: </strong>
          <input
            ref={this.inputsRef.preciseService} value={preciseService || ''}
            onChange={this.getInputFieldChangeHandler('preciseService')} />
        </div> : null}

        {service === 'militaire' ? <RadioButtonQuestion
          style={{marginTop: 25}} name="seaRescue" options={[
            {name: 'Oui', value: 'true'},
            {name: 'Non', value: 'false'},
          ]} value={seaRescue} onChange={this.getFieldChangeHandler('seaRescue')}
          ref={this.inputsRef.seaRescue}>
          Avez-vous une activité de secours en mer ou de SAR&nbsp;?
        </RadioButtonQuestion> : null}

        <CheckboxesQuestion
          style={{marginTop: 25}}
          options={[
            {name: 'CAMU', value: 'camu'},
            {name: "DESC d'urgence", value: 'desc'},
            {name: 'Formation médecin correspondant SAMU', value: 'samu'},
            {name: 'Aucune', value: 'aucune'},
          ]} customName="Autre" onChange={this.getArrayFieldChangeHandler('training')}
          values={training ? training.split(',') : []} ref={this.inputsRef.training}>
          Avez-vous une formation dans le domaine de l’urgence (plusieurs réponses possibles)&nbsp;?
        </CheckboxesQuestion>

        <RadioButtonQuestion
          style={{marginTop: 25}} name="onCallLastYear" options={[
            {name: 'SAU pur', value: 'sau'},
            {name: 'SMUR pur', value: 'smur'},
            {name: 'SAU et SMUR', value: 'sau-smur'},
            {name: 'Médecin correspondant SAMU', value: 'corresp-samu'},
            {name: 'Non', value: 'non'},
          ]} customName="Autre" value={onCallLastYear}
          onChange={this.getFieldChangeHandler('onCallLastYear')}
          ref={this.inputsRef.onCallLastYear}>
          Avez-vous pris des gardes en structure d’urgence sur l’année écoulée&nbsp;?
        </RadioButtonQuestion>
        {onCallLastYear && onCallLastYear !== 'non' ? <div style={{marginTop: 15}}>
          <strong>
            Combien de {onCallLastYear === 'sau' ? 'gardes aux urgences' :
              onCallLastYear === 'smur' ? 'gardes en SMUR' :
                onCallLastYear === 'sau-smur' ? 'gardes en SAU/SMUR' :
                  onCallLastYear === 'corresp-samu' ? 'plages de 12h' : 'gardes'} par mois&nbsp;?
          </strong>
          <NumberInput
            value={numOnCallsPerMonth} ref={this.inputsRef.numOnCallsPerMonth}
            onChange={this.getFieldChangeHandler('numOnCallsPerMonth')} />
        </div> : null}
      </React.Fragment> : null}

      {kind ? null : <div style={{marginTop: 25}}>
        <strong style={{display: 'block', marginBottom: 10}}>
          Depuis combien d'années êtes-vous autonome en SMUR&nbsp;?
        </strong>
        <NumberInput
          value={autonomousYears} ref={this.inputsRef.autonomousYears}
          onChange={this.getFieldChangeHandler('autonomousYears')} />
      </div>}

      {kind === 'medecine-ge' ? <React.Fragment>
        <RadioButtonQuestion
          style={{marginTop: 25}} name="handleRealCA" options={[
            {name: 'Oui', value: 'true'},
            {name: 'Non', value: 'false'},
          ]} value={handleRealCA} onChange={this.getFieldChangeHandler('handleRealCA')}
          ref={this.inputsRef.handleRealCA}>
          Avez-vous déjà pris en charge un arrêt cardiaque réel&nbsp;?
        </RadioButtonQuestion>

        {handleRealCA === 'true' ? <React.Fragment>
          <CheckboxesQuestion
            style={{marginTop: 25}}
            options={[
              {name: 'leader d’intervention au SAU/SMUR/astreinte MCS', value: 'leader'},
              {name: `médecin ${service === 'militaire' ? 'militaire' : 'généraliste'}
                sans déclenchement MCS`, value: 'medecin'},
              {name: 'citoyen lambda', value: 'citoyen'},
              {name: 'stagiaire ou interne', value: 'stagiaire'},
            ]} customName="Autre" onChange={this.getArrayFieldChangeHandler('handleRealCAAs')}
            values={handleRealCAAs ? handleRealCAAs.split(',') : []}
            ref={this.inputsRef.handleRealCAAs}>
            En tant que&nbsp;:
          </CheckboxesQuestion>

          <div style={{marginTop: 25}}>
            <strong style={{display: 'block', marginBottom: 10}}>
              L'année dernière combien d'arrêts cardiaques avez-vous pris en charge&nbsp;?
            </strong>
            <NumberInput
              value={numRea} ref={this.inputsRef.numRea}
              onChange={this.getFieldChangeHandler('numRea')} />
          </div>
        </React.Fragment> : null}

        <RadioButtonQuestion
          style={{marginTop: 25}} name="handleSimulCA" options={[
            {name: 'Oui', value: 'true'},
            {name: 'Non', value: 'false'},
          ]} value={handleSimulCA} onChange={this.getFieldChangeHandler('handleSimulCA')}
          ref={this.inputsRef.handleSimulCA}>
          Avez-vous déjà pris en charge un ACR lors d'une séance de simulation&nbsp;?
        </RadioButtonQuestion>

      </React.Fragment> : <RadioButtonQuestion
        style={{marginTop: 25}} name="numRea" options={[
          {name: 'moins de 5 arrêts cardiaques', value: '0-5'},
          {name: 'de 6 à 10 arrêts', value: '6-10'},
          {name: 'de 11 à 20 arrêts', value: '11-20'},
          {name: 'plus de 20 arrêts', value: '21+'},
        ]} value={numRea} onChange={this.getFieldChangeHandler('numRea')}
        ref={this.inputsRef.numRea}>
        L'année dernière, vous pensez avoir réanimé&nbsp;:
      </RadioButtonQuestion>}

      <RadioButtonQuestion
        style={{marginTop: 25}} name="meanRea" options={[
          {name: 'DSA en mode DSA', value: 'dsa-dsa'},
          {name: 'Scope médicale en mode DSA', value: 'scope-dsa'},
          {name: 'Scope médicale en mode manuel', value: 'scope-manuel'},
          {
            name: 'Scope médicale à 4 brins associée à un DSA en mode DSA',
            value: 'scope-4-brins-dsa',
          },
        ]} value={meanRea} onChange={this.getFieldChangeHandler('meanRea')}
        customName="Cela dépend" ref={this.inputsRef.meanRea}>
        Lors de la réanimation spécialisée des arrêts cardiaques que vous
        menez{kind === 'medecine-ge' ? ' actuellement dans votre pratique la plus fréquente' : ''},
        vous utilisez&nbsp;:
      </RadioButtonQuestion>

      {kind === 'medecine-ge' ? <RadioButtonQuestion
        style={{marginTop: 25}} name="preferredMultiMode" options={[
          {name: 'mode DSA', value: 'dsa'},
          {name: 'mode manuel', value: 'manuel'},
        ]} value={preferredMultiMode} onChange={this.getFieldChangeHandler('preferredMultiMode')}
        ref={this.inputsRef.preferredMultiMode}>
        Si vous disposiez d’un multiparamétrique et que vous aviez le choix, quelle serait votre
        stratégie préférentielle&nbsp;?
      </RadioButtonQuestion> : null}

      {preferredMultiMode ? <div style={{marginTop: 15}}>
        <div stye={{marginBottom: 10}}><strong>
          Pouvez-vous, svp, préciser pourquoi&nbsp;?
        </strong></div>
        <textarea
          value={preferredMultiModeWhy || ''} style={{height: 90, width: '100%'}}
          onChange={this.getInputFieldChangeHandler('preferredMultiModeWhy')} />
      </div> : null}

      <Button isInput={true} type="submit" style={{marginTop: 20}} />
    </form>
  }
}


class EmailForm extends React.Component {
  static propTypes = {
    isForExpert: PropTypes.bool,
    onDone: PropTypes.func.isRequired,
  }

  emailRef = React.createRef()

  handleSubmit = event => {
    event.preventDefault()

    const {onDone} = this.props
    if (!this.emailRef.current) {
      return
    }
    if (!this.emailRef.current.value.trim()) {
      this.emailRef.current.focus()
      return
    }
    onDone({
      email: this.emailRef.current.value.trim(),
    })
  }

  render() {
    const {isForExpert} = this.props
    const style = {
      display: 'block',
      margin: '0 auto',
      maxWidth: 1200,
    }
    return <form onSubmit={this.handleSubmit} style={{padding: '0 20px'}}>
      <div style={style}>
        <fieldset style={{margin: '20px 0'}}>
          <legend>E-mail</legend>
          Nous vous demandons votre adresse mail pour pouvoir relancer ceux qui
          n'ont pas répondu à l'étude autant que pour vous envoyer vos résultats
          comparés à ceux des {isForExpert ? 'autres' : null} expert·e·s, et les
          résultats de l'enquête.
          {isForExpert ? null : ' Vos réponses seront anonymisées avant analyse.'}

          <div style={{marginTop: 15}}>
            <strong>E-mail&nbsp;: </strong>
            <input type="email" ref={this.emailRef} />
          </div>
        </fieldset>

        <Button isInput={true} type="submit" />
      </div>
    </form>
  }
}


class Form extends React.Component {
  static propTypes = {
    isAnonymous: PropTypes.bool,
    kind: PropTypes.oneOf(['expert', 'IADE', 'medecine-ge', 'public', 'rea']),
    onDone: PropTypes.func.isRequired,
    translate: PropTypes.func.isRequired,
  }

  state = {}

  handleSubmit = event => {
    event.preventDefault()

    const {onDone} = this.props
    onDone(_pick(this.state, [
      'age',
      'autonomousYears',
      'doctor',
      'email',
      'experienceYears',
      'gender',
      'handleRealCA',
      'handleRealCAAs',
      'handleSimulCA',
      'iadeYears',
      'meanRea',
      'numRea',
      'numOnCallsPerMonth',
      'onCallLastYear',
      'preciseService',
      'preferredMultiMode',
      'preferredMultiModeWhy',
      'seaRescue',
      'service',
      'training',
    ]))
  }

  endForm = state => this.setState(state)

  renderForm() {
    const {isAnonymous, kind, translate} = this.props
    const isForExpert = kind === 'expert'
    const {email, gender, meanRea} = this.state
    const style = {
      display: 'block',
      margin: '0 auto',
      maxWidth: 1200,
    }
    if (!isAnonymous && !isForExpert && !gender) {
      return <ProfileForm onDone={this.endForm} />
    }
    if (!isAnonymous && !isForExpert && !meanRea) {
      return <ReanimateForm onDone={this.endForm} kind={kind} gender={gender} />
    }
    if (!isAnonymous && !email) {
      return <EmailForm onDone={this.endForm} />
    }
    return <form onSubmit={this.handleSubmit} style={{padding: 20}}>
      <div style={style}>
        <div style={{marginBottom: 20}}>
          {translate('intro')}
          <br />
          {translate('instructions')} {isForExpert ?
            'Prenez bien votre temps pour faire votre choix.' :
            translate('Your reaction time is also collected.')}<br />
          <br />
          {translate(
            'The different rhythms that will follow each other are randomly proposed from ' +
            'a database.')} <u>{translate(
            'Each analysis must be considered totally independent of the previous ones ' +
            '(different patients).')}</u><br />
          {translate('instructions/no-pulse')}
          <br />
          <br />
          {translate(isForExpert ? 'instructions/last-for-expert' : 'instructions/last')}
        </div>

        <div style={{textAlign: 'center'}}>
          <Button isInput={true} type="submit" value="OK" />
        </div>
      </div>
    </form>
  }

  render() {
    return <div style={{fontFamily: 'Helvetica', margin: 'auto', width: 480}}>
      {this.renderForm()}
    </div>
  }
}


export {Form}
