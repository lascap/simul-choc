# Simul Choc

An app to simulate a semi-automatic defibrilator: users will see patient's data
showing on the monitor and need to decide as fast as possible whether to shock
or not.

## Expert Mode

An Expert Mode, accessible through `/expert`, allows to collect the decisions
on every patient in the database by an expert.

There's a quick version of the expert mode that does not store anything nor does
it ask for the expert's name. It's accessible through `/expert?random=5` where
`5` is the number of patients to show.

## Dev

To run the server locally, use:

```sh
yarn vite
```

and access the app at `http://localhost:5173`

## Release & Deploy

Build the compiled app using:

```sh
yarn vite build
```

If everything went well, then you can run:

```sh
bash ./deploy.sh -P default
```

To deploy to `simul-shock.firebaseapp.com` run:

```sh
bash ./deploy.sh -P public
```
